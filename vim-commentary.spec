# There is no better way how to obtain the vimfiles folder location :/
# https://bugzilla.redhat.com/show_bug.cgi?id=844975
%global vimfiles  %{_datadir}/vim/vimfiles

%global appdata_dir %{_datadir}/appdata

Name: vim-commentary
Version: 1.2
Release: 1%{?dist}
Summary: Comment stuff out; takes a motion as a target
Group: Applications/Editors
License: Vim
URL: http://www.vim.org/scripts/script.php?script_id=3695
Source0: https://github.com/tpope/vim-commentary/archive/v%{version}/%{name}-%{version}.tar.gz
# Plug-in AppData for Gnome Software.
# https://github.com/tpope/vim-commentary/pull/52
Source1: vim-commentary.metainfo.xml
Requires: vim-common
Requires(post): %{_bindir}/vim
Requires(postun): %{_bindir}/vim
# Needed for AppData check.
BuildRequires: libappstream-glib
BuildArch: noarch

%description
Comment stuff out. Use gcc to comment out a line (takes a count), gc to
comment out the target of a motion (for example, gcap to comment out a
paragraph), and gc in visual mode to comment out the selection. That's it.

%prep
%setup -q

%build


%install
mkdir -p %{buildroot}%{vimfiles}
cp -pr doc plugin %{buildroot}%{vimfiles}

# Install AppData.
mkdir -p %{buildroot}%{appdata_dir}
install -m 644 %{SOURCE1} %{buildroot}%{appdata_dir}

%check
# Check the AppData add-on to comply with guidelines.
appstream-util validate-relax --nonet %{buildroot}/%{_datadir}/appdata/*.metainfo.xml

%post
vim -c ":helptags %{vimfiles}/doc" -c :q &> /dev/null

%postun
> %{vimfiles}/doc/tags
vim -c ":helptags %{vimfiles}/doc" -c :q &> /dev/null

%files
%doc CONTRIBUTING.markdown README.markdown
%{vimfiles}/doc/*
%{vimfiles}/plugin/*
%{appdata_dir}/vim-commentary.metainfo.xml


%changelog
* Wed Oct 07 2015 Vít Ondruch <vondruch@redhat.com> - 1.2-1
- Initial package.
